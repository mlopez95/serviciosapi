package com.roshka.familiar.pagoservicios.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roshka.familiar.pagoservicios.bean.Servicio;
import com.roshka.familiar.pagoservicios.dao.ServiciosDao;

@Service
public class ServiciosService {
	@Autowired 
	ServiciosDao serviciosDao;
	
	
	public List<Servicio> obtenerSecciones(){
		
		return serviciosDao.obtenerSecciones();
	}
	
	public List<Servicio> obtenerGrupos(){
		
		return serviciosDao.obtenerGrupos();
	}
	
	public List<Servicio> obtenerServicios(){
		
		return serviciosDao.obtenerServicios();
	}
	public int insertarServicio(Servicio servicio){
		return serviciosDao.insertarServicio(servicio);
	}
	public int actualizarServicio(Servicio servicio, Integer id){
		return serviciosDao.updateServicio(servicio, id);
	}
	
	public Servicio unServicio(Integer id){
		
		return serviciosDao.unServicio(id);
	}

}
