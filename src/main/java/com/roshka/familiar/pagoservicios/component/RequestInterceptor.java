package com.roshka.familiar.pagoservicios.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component("requestInterceptor")
public class RequestInterceptor extends HandlerInterceptorAdapter{
	
	private static final Log logger = LogFactory.getLog(RequestInterceptor.class);
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		request.setAttribute("startTime", System.currentTimeMillis());
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		long starTime = (long)request.getAttribute("startTime");
		logger.info("--REQUEST URL : |"+request.getRequestURL()+"| el tiempo que tardo es: "+(starTime-System.currentTimeMillis())+"ms");
		//		super.afterCompletion(request, response, handler, ex);
	}

	
	
	

}
