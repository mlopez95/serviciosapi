package com.roshka.familiar.pagoservicios.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.roshka.familiar.pagoservicios.component.RequestInterceptor;

@Configuration
public class WebMvcConfiguration extends WebMvcConfigurerAdapter{
	@Autowired
	@Qualifier("requestInterceptor")
	private RequestInterceptor requestInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
		registry.addInterceptor(requestInterceptor);
	}

}
