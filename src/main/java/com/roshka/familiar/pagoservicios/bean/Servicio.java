package com.roshka.familiar.pagoservicios.bean;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown=true)
@ApiModel(value = "Servicio", description = "Contiene todos los datos de un Servicio, Grupo o Seccion")
public class Servicio {
	@ApiModelProperty(value = "Id del Servicio", required = false)
	private Integer id;
	@ApiModelProperty(value = "Orden del Servicio", required = false)
	private Integer grupo_orden;
	@ApiModelProperty(value = "Codigo de Movimiento", required = false)
	private Integer codigo_movimiento;
	@ApiModelProperty(value = "Codigo de Transaccion", required = false)
	private Integer codigo_transaccion;
	
	@ApiModelProperty(value = "Nombre del Servicio", required = false)
	private String nombre;
	@ApiModelProperty(value = "Descripcion del Servicio", required = false)
	private String descripcion;
	@ApiModelProperty(value = "URL del Servicio", required = false)
	private String url;
	@ApiModelProperty(value = "Ubicacion del Logo", required = false)
	private String logo;
	@ApiModelProperty(value = "Texto de ayuda", required = false)
	private String ayuda;
	@ApiModelProperty(value = "Nivel del servicio", required = false)
	private String nivel;
	@ApiModelProperty(value = "Grupo al que pertenece el servicio", required = false)
	private String grupo;
	@ApiModelProperty(value = "Tipo de servicio", required = false)
	private String tipo;
	@ApiModelProperty(value = "Estado del servicio", required = false)
	private String activo;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGrupo_orden() {
		return grupo_orden;
	}
	public void setGrupo_orden(Integer grupo_orden) {
		this.grupo_orden = grupo_orden;
	}
	public Integer getCodigo_movimiento() {
		return codigo_movimiento;
	}
	public void setCodigo_movimiento(Integer codigo_movimiento) {
		this.codigo_movimiento = codigo_movimiento;
	}
	public Integer getCodigo_transaccion() {
		return codigo_transaccion;
	}
	public void setCodigo_transaccion(Integer codigo_transaccion) {
		this.codigo_transaccion = codigo_transaccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getAyuda() {
		return ayuda;
	}
	public void setAyuda(String ayuda) {
		this.ayuda = ayuda;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	
	
	

}
