package com.roshka.familiar.pagoservicios.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.roshka.familiar.pagoservicios.bean.Servicio;

public class ServicioRowMapper implements RowMapper<Servicio> {

	@Override
	public Servicio mapRow(ResultSet rs, int arg1) throws SQLException {
		
		Servicio servicio = new Servicio();
		servicio.setId(rs.getInt("id"));
		servicio.setNombre(rs.getString("nombre"));
		servicio.setDescripcion(rs.getString("descripcion"));
		servicio.setUrl(rs.getString("url"));
		servicio.setCodigo_transaccion(rs.getInt("codigo_transaccion"));
		servicio.setCodigo_movimiento(rs.getInt("codigo_movimiento"));
		servicio.setGrupo(rs.getString("grupo"));
		servicio.setLogo(rs.getString("logo"));
		servicio.setAyuda(rs.getString("ayuda"));
		servicio.setGrupo_orden(rs.getInt("grupo_orden"));
		servicio.setActivo(rs.getString("activo"));
		servicio.setTipo(rs.getString("tipo"));
		servicio.setNivel(rs.getString("nivel"));
		
		
		return servicio;
	}

}
