package com.roshka.familiar.pagoservicios.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource(value="classpath:consultas.properties") // default
//@PropertySource(value="file:C:/Users/na_fi_000/Documents/Familiar/FamiliarPagoServicios/servicios/src/main/resources/consultas.properties",ignoreResourceNotFound=true) //por si sea tomcat
//@PropertySource(value="file:${config.dir}/consultas.properties",ignoreResourceNotFound=true) //necesita la ruta como argumento
public class ConsultasEndpoints {
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	   return new PropertySourcesPlaceholderConfigurer();
	}
	
	private static String selecSecciones;
	private static String selectGrupos;
	private static String selectServicios;
	private static String insertServicios;
	private static String updateServicios;
	
	
	public static String getSelecSecciones() {
		return selecSecciones;
	}
	
	@Value("${secciones}")
	public static void setSelecSecciones(String secciones) {
		ConsultasEndpoints.selecSecciones = secciones;
	}
	public static String getSelectGrupos() {
		return selectGrupos;
	}
	
	@Value("${grupos}")
	public static void setSelectGrupos(String grupos) {
		ConsultasEndpoints.selectGrupos = grupos;
	}
	public static String getSelectServicios() {
		return selectServicios;
	}
	
	@Value("${servicios}")
	public static void setSelectServicios(String servicios) {
		ConsultasEndpoints.selectServicios = servicios;
	}
	public static String getInsertServicios() {
		return insertServicios;
	}
	
	@Value("${insert}")
	public static void setInsertServicios(String insert) {
		ConsultasEndpoints.insertServicios = insert;
	}
	public static String getUpdateServicios() {
		return updateServicios;
	}
	
	@Value("${update}")
	public static void setUpdateServicios(String update) {
		ConsultasEndpoints.updateServicios = update;
	}
	
	

}
