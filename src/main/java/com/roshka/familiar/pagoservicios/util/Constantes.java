package com.roshka.familiar.pagoservicios.util;

public class Constantes {
	public static final String PARAMETRO_DE_SEGURIDAD = "api_key";
	public static final String GRUPOS = "G";
	
	//constantes para los insert
	public static final String URL_SERVICIOS = "pagoServicio.jsf?servicioId=";
	public static final String URL_LOGO = "https://ssecure.familiar.com.py/icons/pagoServicio/";
	
	public static final String SELECT_SECCIONES = "select * from bw_pago_servicios where tipo='G' and grupo not like '%^%'";
	public static final String SELECT_GRUPOS = "select * from bw_pago_servicios where tipo='G' and grupo like '%^%'";
	public static final String SELECT_SERVICIOS = "select * from bw_pago_servicios where tipo!='G'";
	public static final String INSERT_SERVICIO = "insert into bw_pago_servicios (id, nombre, ayuda, url, grupo, nivel, activo, logo, codigo_movimiento) values (?,?,?,?,?,?,?,?,?)";
	public static final String UPDATE_SERVICIO = "UPDATE bw_pago_servicios set id = ?, nombre = ?, descripcion= ?, url= ?, logo=?, ayuda= ?, nivel= ?, activo= ?, grupo= ?, grupo_orden= ?, codigo_transaccion= ?, codigo_movimiento= ?, tipo=? where id = ?;";
	
	public static final String SELECT_UN = "select * from bw_pago_servicios where id=?";

}
