package com.roshka.familiar.pagoservicios.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.roshka.familiar.pagoservicios.bean.Servicio;
import com.roshka.familiar.pagoservicios.util.Constantes;
import com.roshka.familiar.pagoservicios.util.ServicioRowMapper;

@Repository
public class ServiciosDao {
	private static final Logger logger = LoggerFactory.getLogger(ServiciosDao.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<Servicio> obtenerSecciones() {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.SELECT_SECCIONES);
		
		return jdbcTemplate.query(Constantes.SELECT_SECCIONES, new ServicioRowMapper());
	}
	
	public List<Servicio> obtenerGrupos() {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.SELECT_GRUPOS);
		
		return jdbcTemplate.query(Constantes.SELECT_GRUPOS, new ServicioRowMapper());
	}
	
	public List<Servicio> obtenerServicios() {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.SELECT_SERVICIOS);
		return jdbcTemplate.query(Constantes.SELECT_SERVICIOS, new ServicioRowMapper());
	}
	
	public int insertarServicio(Servicio servicio) {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.INSERT_SERVICIO);
		
		return jdbcTemplate.update(Constantes.INSERT_SERVICIO,
				servicio.getId(),
				servicio.getNombre(),
				servicio.getAyuda(),
				Constantes.URL_SERVICIOS+servicio.getId(),
				servicio.getGrupo(),
				servicio.getNivel(),
				servicio.getActivo(),
				Constantes.URL_LOGO+servicio.getLogo(),
				servicio.getCodigo_movimiento());
	}
	
	public int updateServicio(Servicio servicio, Integer id) {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.UPDATE_SERVICIO);
		
		return jdbcTemplate.update(Constantes.UPDATE_SERVICIO,
				servicio.getId(),
				servicio.getNombre(),
				servicio.getDescripcion(),
				Constantes.URL_SERVICIOS+servicio.getId(),
				Constantes.URL_LOGO+servicio.getLogo(),
				servicio.getAyuda(),
				servicio.getNivel(),
				servicio.getActivo(),
				servicio.getGrupo(),
				servicio.getGrupo_orden(),
				servicio.getCodigo_transaccion(),
				servicio.getCodigo_movimiento(),
				servicio.getTipo(),
				id);
	}
	
	public Servicio unServicio(Integer id) {
		logger.info("Se realizará la siguiente consulta : ");
		logger.info(Constantes.SELECT_UN);
		List<Servicio> lista = jdbcTemplate.query(Constantes.SELECT_UN, new ServicioRowMapper(),id);;
		return lista.get(0);
	}
	

}
