package com.roshka.familiar.pagoservicios.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.roshka.familiar.pagoservicios.bean.Servicio;
import com.roshka.familiar.pagoservicios.service.ServiciosService;
import com.roshka.familiar.pagoservicios.util.Constantes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(value = "servicios", description = "Servicios API", produces = "application/json")
public class PagoServiciosController {
	private static final Logger logger = LoggerFactory.getLogger(PagoServiciosController.class);
	
	@Autowired 
	ServiciosService serviciosService;
	
	@ApiOperation(value = "obtener-secciones", notes = "Retorna todas las secciones existentes")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/obtener-secciones",method = RequestMethod.GET)
	public List<Servicio> obtenerSecciones(
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey ){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.obtenerSecciones();
	}
	
	@ApiOperation(value = "obtener-grupos", notes = "Retorna todas las grupos existentes")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/obtener-grupos",method = RequestMethod.GET)
	public List<Servicio> obtenerGrupos(
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey ){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.obtenerGrupos();
	}
	
	@ApiOperation(value = "obtener-servicios", notes = "Retorna todas las servicios existentes")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/obtener-servicios",method = RequestMethod.GET)
	public List<Servicio> obtenerServicios(
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey ){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.obtenerServicios();
	}
	
	@ApiOperation(value = "actualizar-servicios", notes = "Retorna la cantidad de registros actualizados")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/actualizar-servicio",method = RequestMethod.POST)
	public int actualizarServicios(
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey,
			@RequestParam(value="id",required=false) Integer id, @RequestBody Servicio servicio ){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.actualizarServicio(servicio, id);
	}
	
	@ApiOperation(value = "insertar-servicio", notes = "Retorna la cantidad de servicios insertados")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/insertar-servicio",method = RequestMethod.POST)
	public int insertarServicios(
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey,@RequestBody Servicio servicio ){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.insertarServicio(servicio);
	}
	
	@ApiOperation(value = "/un-servicio/{id}", notes = "Retorna un servicio por ID ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Exits one info at least")
    })
	@RequestMapping(value="/un-servicio/{id}",method = RequestMethod.GET,produces = "application/json")
	public Servicio unSecciones(@ApiParam(value="id", required=true) @PathVariable("id") Integer id,
			@RequestParam(value=Constantes.PARAMETRO_DE_SEGURIDAD,required=false) String apikey){
//		methodo para validar el apikey
		if(apikey==null)
			logger.error("Necesita un apikey..");
		
		return serviciosService.unServicio(id);
	}
}
